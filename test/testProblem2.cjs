const path = require('path');
const problem2 = require('../problem2.cjs');

const lipsum = path.join(__dirname, './lipsum.txt');

problem2(lipsum).then((value) => {
    console.log(value);
})
    .catch((error) => {
        console.error(error);
    })