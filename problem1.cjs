const fs = require('fs');

function problem1(folder) {
    return directory(folder).then((folder) => {
        return files(folder);
    })
        .then((arrayFile) => {
            filesDeleted(arrayFile);
        })
        .catch((err) => {
            if (err) {
                reject(err);
            }
        })
}

function directory(folder) {
    return new Promise((resolve, reject) => {
        fs.mkdir(folder, { recursive: true }, (err) => {
            if (err) {
                reject(err);
            } else {
                console.log("created directory");
                resolve(folder); //
            }
        });
    });
}

function files(folder) {
    return new Promise((resolve, reject) => {
        let number = Math.floor(Math.random() * 10);
        let arrayFile = Array(number).fill(0).map((element, index) => {
            return `${folder}/file${index}.json`;
        });
        let eachFile = arrayFile.map((file, index) => {
            return createFiles(file, index)
        });
        Promise.all(eachFile).then((values) => {
            console.log(values);
            console.log("files created sucessfully")
            resolve(arrayFile); //
        })
            .catch((err) => {
                reject(err);
            })
    })
}


function createFiles(file, index) {
    return new Promise((resolve, reject) => {
        fs.writeFile(file, 'created file', (err) => {
            if (err) {
                reject(err);
            } else {
                resolve(`file${index}.json file created`);
            }
        })
    })
}


function filesDeleted(arrayFile) {
    return new Promise((resolve, reject) => {
        const deletedPromises = arrayFile.map((fileName, index) => {
            return eachFileDeleted(fileName, index);
        });
        Promise.all(deletedPromises).then((fromresolve) => {
            console.log(fromresolve);
            console.log('files deleted successfully');
            resolve();
        })
            .catch((err) => {
                reject(err);
            })
    })
}


function eachFileDeleted(file, index) {
    return new Promise((resolve, reject) => {
        fs.unlink(file, (err) => {
            if (err) {
                reject(err);
            } else {
                resolve(`file${index}.json deleted`);
            }
        })
    })
}


module.exports = problem1;