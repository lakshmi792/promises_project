const fs = require('fs');
const path = require('path');

const sentencesDataFilePath = path.join(__dirname, './sentences.txt');
const fileNames = path.join(__dirname, './filenames.txt');
const upperCase = path.join(__dirname, './uppercase.txt');
const sortedSentencesPath = path.join(__dirname, './sortedFile.txt');

function problem2(lipsum) {
    return new Promise((resolve, reject) => {
        lipsumData(lipsum)
            .then((data) => {
                return upperCaseData(data);
            }).then((data) => {
                return splitThesentencesLowerCase(data);
            }).then((data) => {
                return sortedSentences(data);
            }).then((value) => {
                return deleteFiles(fileNames);
            })
            .then((value) => {
                console.log(value);
                resolve('success')
            })
            .catch((error) => {
                if (error) {
                    reject(error);
                }
            })
    })
}

function lipsumData(lipsum) {
    return new Promise((resolve, reject) => {
        fs.readFile(lipsum, 'utf-8', (error, data) => {
            if (error) {
                reject(error);
            } else {
                console.log('reading lipsum file');
                resolve(data);
            }
        })
    })
}

function upperCaseData(data) {
    return new Promise((resolve, reject) => {
        const convertedData = data.toUpperCase();
        fs.writeFile(upperCase, convertedData.toString(), (err) => {
            if (err) {
                reject(err);
            } else {
                console.log('converted to uppercase');
                FileName(upperCase + '\n')
                    .then((value) => {
                        console.log(value);
                    })
                    .catch((error) => {
                        console.error(error);
                    });
                resolve(data);
            }
        })
    })
}

function FileName(data, fileNames = path.join(__dirname, './filenames.txt')) {
    return new Promise((resolve, reject) => {
        fs.appendFile(fileNames, data.toString(), (err) => {
            if (err) {
                reject(err);
            } else {
                resolve('added file name');
            }
        })
    })
}

function splitThesentencesLowerCase(data) {
    return new Promise((resolve, reject) => {
        const convertedData = data.toLowerCase().split('. ').join('.\n');
        fs.writeFile(sentencesDataFilePath, convertedData.toString(), (err) => {
            if (err) {
                reject(err);
            } else {
                console.log('Sentences File created sucessfully');
                FileName(sentencesDataFilePath + '\n')
                    .then((message) => {
                        console.log(message);
                    })
                    .catch((err) => {
                        console.error(err);
                    });
                resolve(convertedData);
            }
        })
    })
}

function sortedSentences(data) {
    return new Promise((resolve, reject) => {
        const sortedDataSentences = data.split('\n').sort().slice(3);
        fs.writeFile(sortedSentencesPath, sortedDataSentences.join('\n').toString(), (err) => {
            if (err) {
                reject(err);
            } else {
                FileName(sortedSentencesPath)
                    .then((message) => {
                        console.log(message);
                    })
                    .catch((err) => {
                        console.error(err);
                    })
                resolve('created file');
            }
        })
    })
}

function deleteFiles(fileNamesTxtpath) {
    return new Promise((resolve, reject) => {
        fs.readFile(fileNamesTxtpath, 'utf-8', (err, data) => {
            if (err) {
                reject(err);
            } else {
                const fileNamesPromises = data.split('\n').map((filename) => {
                    return deleteEachFile(filename);
                });
                Promise.all(fileNamesPromises)
                    .then((message) => {
                        console.log(message);
                        resolve('deleted files');
                    })
                    .catch((err) => {
                        console.error(err);
                    })
            }
        })
    })
}

function deleteEachFile(fileName) {
    return new Promise((resolve, reject) => {
        fs.unlink(fileName, (err) => {
            if (err) {
                reject(err);
            } else {
                resolve(`${fileName} deleted`);
            }
        })
    })
}

module.exports = problem2;